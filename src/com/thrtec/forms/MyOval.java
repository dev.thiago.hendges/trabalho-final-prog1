package com.thrtec.forms;

import com.thrtec.util.PositionUtil;

import java.awt.*;

public class MyOval extends MyShape {

    // construtor que recebe uma cor e cria um shape com posição aleatória
    public MyOval(Color color) {
        super(color, PositionUtil.getSafeRandomPositionPoint(), PositionUtil.getSafeRandomPositionPoint());
    }

    // metodo para desenhar o shape, é necessário pois a MyOval extende MyShape que implementa a interface de desenho de shape
    @Override
    public void draw(Graphics graphics) {
        // seta a cor
        graphics.setColor(super.getColor());

        // desenha
        graphics.drawOval(super.getOriginPoint().getX(),
                super.getOriginPoint().getY(),
                Math.abs(super.getOriginPoint().getX() - super.getDestPoint().getX()),
                Math.abs(super.getOriginPoint().getY() - super.getDestPoint().getY())
        );
    }
}
