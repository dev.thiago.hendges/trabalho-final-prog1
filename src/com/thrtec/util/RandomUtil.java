package com.thrtec.util;

import java.util.Random;

public class RandomUtil {

    private static final Random random = new Random();

    // Metodo generico para gerar um numero inteiro aleatório com offset
    public static Integer getRandomInteger(Integer max, Integer offset) {
        return offset > max ? offset : offset + random.nextInt(max);
    }

    // Metodo generico para gerar um numero inteiro aleatório
    public static Integer getRandomInteger(Integer max) {
        return random.nextInt(max);
    }
}
