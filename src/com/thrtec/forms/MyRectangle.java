package com.thrtec.forms;

import com.thrtec.util.PositionUtil;

import java.awt.*;

public class MyRectangle extends MyShape {

    // construtor que recebe uma cor e cria um shape com posição aleatória
    public MyRectangle(Color color) {
        super(color, PositionUtil.getSafeRandomPositionPoint(), PositionUtil.getSafeRandomPositionPoint());
    }

    // metodo para desenhar o shape, é necessário pois a MyRectangle extende MyShape que implementa a interface de desenho de shape
    @Override
    public void draw(Graphics graphics) {
        // seta cor
        graphics.setColor(super.getColor());

        // desenha o retangulo
        graphics.drawRect(super.getOriginPoint().getX(),
                super.getOriginPoint().getY(),
                Math.abs(super.getOriginPoint().getX() - super.getDestPoint().getX()),
                Math.abs(super.getOriginPoint().getY() - super.getDestPoint().getY())
        );
    }

}
