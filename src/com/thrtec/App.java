package com.thrtec;

import com.thrtec.panels.DrawFormsPanel;
import com.thrtec.panels.DrawInfosPanel;

import javax.swing.*;

public class App {

    public static final String WINDOW_TITLE = "Trabalho final PROGRAMAÇÃO 1 - Thiago Rocha"; // titulo da janela
    public static final Boolean WINDOW_RESIZABLE = false;  // define se a janela é redimensionavel
    public static final Integer WINDOW_SIZE_X = 600; // largura da janela
    public static final Integer WINDOW_SIZE_Y = 400; // altura da janela
    public static final Integer MENU_SIZE_Y = 70; // altura do menu de informações

    public static void main(String[] args) {
        DrawFormsPanel formsPanel = new DrawFormsPanel(); // cria painel de formas
        DrawInfosPanel infosPanel = new DrawInfosPanel(formsPanel.getQuantityLine(), formsPanel.getQuantityRectangle(), formsPanel.getQuantityOval()); // cria painel de informaçoes

        // cria o frame e adiciona os paineis para visualização
        JFrame jFrame = new JFrame();
        jFrame.add(infosPanel);
        jFrame.add(formsPanel);

        // configura o frame de acordo com as variveis setadas acima
        configureFrame(jFrame);

        // mostra o frame
        jFrame.setVisible(true);
    }

    private static void configureFrame(JFrame jFrame) {
        jFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        jFrame.setSize(WINDOW_SIZE_X, WINDOW_SIZE_Y);
        jFrame.setResizable(WINDOW_RESIZABLE);
        jFrame.setTitle(WINDOW_TITLE);
    }

}
