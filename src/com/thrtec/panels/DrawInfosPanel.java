package com.thrtec.panels;

import com.thrtec.App;

import javax.swing.*;
import java.awt.*;
import java.util.Arrays;
import java.util.List;

public class DrawInfosPanel extends JPanel {

    // amostra jpanel
    @Override
    public void paintComponent(Graphics graphics) {
        super.paintComponent(graphics);
    }

    public DrawInfosPanel(Integer quantityLine, Integer quantityRectangle, Integer quantityOval) {
        configureInfosPanel();
        configureInfosPanelComponents(quantityLine, quantityRectangle, quantityOval);
    }

    private void configureInfosPanelComponents(Integer quantityLine, Integer quantityRectangle, Integer quantityOval) {

        // cria lista de componentes de informações
        List<Object> panelComponents = Arrays.asList(
                new JLabel(String.format("Largura da tela: %d px", App.WINDOW_SIZE_X)),
                new JLabel(String.format("Altura da tela: %d px", App.WINDOW_SIZE_Y)),
                new JLabel(String.format("Quantidade de linhas: %d", quantityLine)),
                new JLabel(String.format("Quantidade de retângulos: %d", quantityRectangle)),
                new JLabel(String.format("Quantidade de ovais: %d", quantityOval)),
                new JLabel("202102 - PROGRAMAÇÃO I")
        );

        // Adiciona cada componente de informação (label) ao painel
        panelComponents.forEach(component -> add((JComponent) component));
    }

    private void configureInfosPanel() {
        // define o tamanho do painel
        setSize(App.WINDOW_SIZE_X - 15, App.MENU_SIZE_Y - 10);

        // define aonde o painel deve ser exibido no frame
        setLocation(0, App.WINDOW_SIZE_Y - (App.MENU_SIZE_Y + 38));

        // define o layout que será utilizado
        setLayout(new GridLayout(2, 5));

        // cria uma borda preta para o painel
        setBorder(BorderFactory.createLineBorder(Color.black));
    }
}
