package com.thrtec.util;

import java.awt.*;

public class ColorUtil {

    // Metodo generico para criar uma cor aleatória
    public static Color getRandomColor() {
        return new Color(
                RandomUtil.getRandomInteger(256),
                RandomUtil.getRandomInteger(256),
                RandomUtil.getRandomInteger(256)
        );
    }

}
