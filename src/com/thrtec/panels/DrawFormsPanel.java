package com.thrtec.panels;

import com.thrtec.forms.MyLine;
import com.thrtec.forms.MyOval;
import com.thrtec.forms.MyRectangle;
import com.thrtec.forms.MyShape;
import com.thrtec.util.RandomUtil;

import javax.swing.*;
import java.awt.*;

import static com.thrtec.util.ColorUtil.getRandomColor;

public class DrawFormsPanel extends JPanel {

    // define array de linhas com tamanho aleatório
    private MyShape[] lines = new MyLine[RandomUtil.getRandomInteger(20, 5)];

    // define array de retangulos com tamanho aleatório
    private MyShape[] rectangles = new MyRectangle[RandomUtil.getRandomInteger(10, 5)];

    // define array de ovais com tamanho aleatório
    private MyShape[] ovals = new MyOval[RandomUtil.getRandomInteger(10, 5)];

    // variaveis que armazenam os tamanhos de cada array
    private Integer quantityLine = lines.length;
    private Integer quantityRectangle = rectangles.length;
    private Integer quantityOval = ovals.length;

    public DrawFormsPanel() {
        // define a cor do fundo do painel de formas com a cor branca
        setBackground(Color.WHITE);

        // preenche cada array de forma com a respectiva forma com cor aleatória
        for (int i = 0; i < quantityLine; i++) {
            lines[i] = new MyLine(getRandomColor());
        }
        for (int i = 0; i < quantityRectangle; i++) {
            rectangles[i] = new MyRectangle(getRandomColor());
        }
        for (int i = 0; i < quantityOval; i++) {
            ovals[i] = new MyOval(getRandomColor());
        }
    }

    @Override
    public void paintComponent(Graphics graphics) {
        super.paintComponent(graphics);
        for (MyShape line : lines) {
            line.draw(graphics);
        }
        for (MyShape rectangle : rectangles) {
            rectangle.draw(graphics);
        }
        for (MyShape oval : ovals) {
            oval.draw(graphics);
        }
    }

    public Integer getQuantityLine() {
        return quantityLine;
    }

    public Integer getQuantityRectangle() {
        return quantityRectangle;
    }

    public Integer getQuantityOval() {
        return quantityOval;
    }
}
