package com.thrtec.util;

import com.thrtec.App;
import com.thrtec.positions.PositionPoint;

public class PositionUtil {

    // metodo generico para gerar uma posição aleatória visivel na tela
    public static PositionPoint getSafeRandomPositionPoint() {
        Integer x = RandomUtil.getRandomInteger(App.WINDOW_SIZE_X - 20);
        Integer y = RandomUtil.getRandomInteger(App.WINDOW_SIZE_Y - App.MENU_SIZE_Y - 38);

        return new PositionPoint(x, y);
    }

}
