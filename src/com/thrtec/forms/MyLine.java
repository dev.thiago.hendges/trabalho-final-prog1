package com.thrtec.forms;

import com.thrtec.util.PositionUtil;

import java.awt.*;

public class MyLine extends MyShape {

    // construtor que recebe uma cor e cria um shape com posição aleatória
    public MyLine(Color color) {
        super(color, PositionUtil.getSafeRandomPositionPoint(), PositionUtil.getSafeRandomPositionPoint());
    }

    // metodo para desenhar o shape, é necessário pois a MyLine extende MyShape que implementa a interface de desenho de shape
    @Override
    public void draw(Graphics graphics) {
        // seta a cor
        graphics.setColor(super.getColor());

        // desenha
        graphics.drawLine(super.getOriginPoint().getX(),
                super.getOriginPoint().getY(),
                super.getDestPoint().getX(),
                super.getDestPoint().getY()
        );
    }
}
