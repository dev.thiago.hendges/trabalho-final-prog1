package com.thrtec.forms;

import java.awt.*;

public interface DrawShape {

    // metodo para desenhar o shape, toda classe com implementação de MyShape deverá ter
    void draw(Graphics graphics);

}
