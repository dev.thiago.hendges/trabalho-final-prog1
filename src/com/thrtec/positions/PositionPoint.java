package com.thrtec.positions;

// Objeto que serve como cordenada
public class PositionPoint {

    // posições em eixo x,y
    private Integer x;
    private Integer y;

    public PositionPoint(final Integer x, final Integer y) {
        this.x = x;
        this.y = y;
    }

    public Integer getX() {
        return x;
    }

    public Integer getY() {
        return y;
    }
}
