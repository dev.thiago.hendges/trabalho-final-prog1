package com.thrtec.forms;

import com.thrtec.positions.PositionPoint;

import java.awt.*;

// classe abstrata que define um shape
public abstract class MyShape implements DrawShape {

    private Color color;
    private PositionPoint originPoint;
    private PositionPoint destPoint;

    protected MyShape(Color color, PositionPoint originPoint, PositionPoint destPoint) {
        this.color = color;
        this.originPoint = originPoint;
        this.destPoint = destPoint;
    }

    public Color getColor() {
        return color;
    }

    public PositionPoint getOriginPoint() {
        return originPoint;
    }

    public PositionPoint getDestPoint() {
        return destPoint;
    }

}
